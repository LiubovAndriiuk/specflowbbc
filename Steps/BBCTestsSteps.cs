﻿
using BBCSpecFlowTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace BBCSpecFlowTest.Steps
{
    [Binding]
    public sealed class BbcTestsSteps
    {
        protected IWebDriver driver;
        private NewsPage newsPage;
        private string pageTitle;
        private string shareLink;

        [BeforeScenario]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
        }

        [Given(@"the BBC HomePage is open")]
        public void GivenTheBBCHomePageIsOpen()
        {
            driver.Navigate().GoToUrl("https://www.bbc.com/");
        }

        [When(@"I go to NewsHeading")]
        public void WhenIGoToNewsHeading()
        {
            newsPage = new HomePage(driver)
                  .ClickOnTheNewsHeading()
                  .ClickOnThePopUpSingIn();
        }

        [When(@"click on ""(.*)"" tab")]
        public void WhenClickOnTab(string tab)
        {
            newsPage.ClickOnTab(tab);
        }

        [When(@"click the first article")]
        public void WhenClickTheFirstArticle()
        {
            newsPage.clickOnHeaderArticle();
        }

        [When(@"click ""(.*)""")]
        public void WhenClick(string tab)
        {            
            pageTitle = newsPage
                .ClickOnButton(tab)
                .getArticleText();
        }

        [When(@"copy the link")]
        public void WhenCopyTheLink()
        {
            shareLink = newsPage.getShareLink();
        }

        [When(@"navigate the link")]
        public void WhenNavigateTheLink()
        {
            driver.Navigate().GoToUrl(shareLink);
        }


        [Then(@"the same article is open")]
        public void ThenTheSameArticleIsOpen()
        {
            Assert.AreNotEqual(pageTitle, newsPage.getArticleText(), "Titles should be equal");
        }



        [When(@"go to ""(.*)""")]
        public void WhenGoToHowToShareWithBBCNews(string elem)
        {
            newsPage.ClickOnTheElement(elem);
        }
        
        [When(@"go to “How to share with BBC news”")]
        public void WhenGoToHowToShareWithBBCNews()
        {
            newsPage.ClickOnHowToShareWithBBCNewsPage();
        }

        [When(@"fill form share with BBC news in the information on the bottom")]
        public void WhenFillFormShareWithBBCNewsInTheInformationOnTheBottom(Table table)
        {
            newsPage.GetForm().FillFormHowToShareWithBBCNews(ToDictionary(table));
        }

        [When(@"fill form send your question in the information on the bottom")]
        public void WhenFillFormSendYourQuestionInTheInformationOnTheBottom(Table table)
        {
            newsPage.GetForm().FillFormSendYourQuestion(ToDictionary(table));
        }

        [Then(@"the story is not sent")]
        public void ThenTheStoryIsNotSent()
        {
            Assert.IsTrue(newsPage.GetForm().IsStorySubmited());
        }

        private Dictionary<string, string> ToDictionary(Table table)
        {
            var dictionary = new Dictionary<string, string>();
            foreach (var row in table.Rows)
            {
                dictionary.Add(row[0], row[1]);
            }
            return dictionary;
        }

        [Then(@"Checks the name of the headline article")]
        public void ThenChecksTheNameOfTheHeadlineArticle()
        {
            newsPage.GetTextHeaderArticle();
            Assert.AreNotEqual(ClassWithData.expectedTextHeadingArticle, newsPage.GetTextHeaderArticle());
        }

        [Then(@"Checks secondary article titles")]
        public void ThenChecksSecondaryArticleTitles()
        {
            Assert.AreNotEqual(ClassWithData.expectedTextHeadingArticle, newsPage.SecondaryTitleArticles());
        }

        [When(@"Get Stores the text of the Category link of the headline article and Enter this text in the Search bar")]
        public void WhenGetStoresTheTextOfTheCategoryLinkOfTheHeadlineArticleAndEnterThisTextInTheSearchBar()
        {
            newsPage.EnterTextCategoryLinkInTheSearchInput(newsPage.GetTextCategoryLink());
        }

        [Then(@"Check the name of the first article against a specified value")]
        public void ThenCheckTheNameOfTheFirstArticleAgainstASpecifiedValue()
        {
          Assert.IsFalse(newsPage.FirstResultArticleIsCorrect(ClassWithData.secondHeaderArticle));
        }

        [When(@"go to “Coronavirus: Send us your questions”")]
        public void WhenGoToCoronavirusSendUsYourQuestions()
        {
            newsPage.ClickOnTheSendUsQuestionsLink();
        }

        [Then(@"the submission did not work")]
        public void ThenTheSubmissionDidNotWork()
        {
            Assert.IsTrue(newsPage.IsDisplayedrrorMessageOnTheQuestionBBC());
            
        }

        [AfterScenario]
        public void CleanUp()
        {
            driver.Close();
        }

    }
}
