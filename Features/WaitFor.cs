﻿
using OpenQA.Selenium;
using System;

namespace BBCAutomationTesting.BBL
{
    internal static class WaitFor
    {
        internal static IWebDriver driver;

        internal static void WaitForSeconds(int second)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(second);
        }

    }
}
