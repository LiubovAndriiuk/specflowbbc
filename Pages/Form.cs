﻿using BBCSpecFlowTest.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;

namespace BBCSpecFlowTest.Features
{
   public class Form : BasePage
    {
        protected internal Form(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
        }

        [FindsBy(How = How.XPath, Using = ("//input[@placeholder='Name']"))]
        private IWebElement NameInput;

        [FindsBy(How = How.XPath, Using = ("//input[@placeholder='Email address']"))]
        private IWebElement EmailInput;

        [FindsBy(How = How.XPath, Using = ("//textarea[@placeholder='Tell us your story. ']"))]
        private IWebElement StoryInput;

        [FindsBy(How = How.XPath, Using = ("//input[@placeholder='Contact number ']"))]
        private IWebElement ContactInput;

        [FindsBy(How = How.XPath, Using = ("//input[@placeholder='Location ']"))]
        private IWebElement LocationInput;

        [FindsBy(How = How.XPath, Using = ("//div[@class='long-text-input-container']"))]
        private IWebElement QuestionInput;

        [FindsBy(How = How.XPath, Using = ("//input[@placeholder='Age']"))]
        private IWebElement AgeInput;

        [FindsBy(How = How.XPath, Using = ("//input[@placeholder='Postcode']"))]
        private IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = ("//input[@placeholder='Telephone number']"))]
        private IWebElement TelephoneNumberInput;

        public Form FillFormHowToShareWithBBCNews(Dictionary<string, string> dictionary)
        {
            StoryInput.SendKeys(dictionary["Story"]);
            NameInput.SendKeys(dictionary["Name"]);
            EmailInput.SendKeys(dictionary["Email Address"]);
            ContactInput.SendKeys(dictionary["Contact number"]);
            LocationInput.SendKeys(dictionary["Location"]);
            return this;
        }
        public Form FillFormSendYourQuestion(Dictionary<string, string> dictionary)
        {
            QuestionInput.SendKeys(dictionary["Qestion"]);
            NameInput.SendKeys(dictionary["Name"]);
            EmailInput.SendKeys(dictionary["Email"]);
            AgeInput.SendKeys(dictionary["Age"]);
            PostcodeInput.SendKeys(dictionary["Postcode"]);
            TelephoneNumberInput.SendKeys(dictionary["Telphone"]);
            return this;
        }
            public bool IsStorySubmited()
        {
            return StoryInput.Displayed;
        }
    }
}
