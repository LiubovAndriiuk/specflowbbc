﻿using BBCAutomationTesting.BBL;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;

namespace BBCSpecFlowTest.Pages
{
    public class HomePage : BasePage
    {
        private readonly IWebDriver driver;

        public HomePage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
            
           PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//div[@id='orb-nav-links']//a[contains(text(),'News')]")]
        private IWebElement NewsHeading;

        public NewsPage ClickOnTheNewsHeading()
        {
           
            NewsHeading.Click();
            return new NewsPage(driver);
        }

    }
}