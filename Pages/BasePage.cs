﻿using OpenQA.Selenium;

namespace BBCSpecFlowTest.Pages
{
    public class BasePage
    {
        protected IWebDriver driver;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public NewsPage GetHeader() => new NewsPage(driver);
    }
}
