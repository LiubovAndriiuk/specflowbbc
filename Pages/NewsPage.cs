﻿using BBCSpecFlowTest.Features;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCSpecFlowTest.Pages
{
    public class NewsPage : BasePage
    {
        public NewsPage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
        }
        [FindsBy(How = How.XPath, Using = "//button[@class='sign_in-exit']")]
        private IWebElement PopUpSingIn;

        [FindsBy(How = How.XPath, Using = "//div[@data-entityid='container-top-stories#1']//a")]
        private IWebElement TextHeaderArticle;

        [FindsBy(How = How.XPath, Using = "//li[@class='nw-c-promo-meta']/a")]
        private IWebElement TextCategoryLink;

        [FindsBy(How = How.XPath, Using = "//input[@id='orb-search-q']")]
        private IWebElement InputSearch;

        [FindsBy(How = How.XPath, Using = "//p[contains(@class, 'PromoHeadline')]/a")]
        private IWebElement SearchResultFirstArticle;

        [FindsBy(How = How.XPath, Using = "//button[@id='orb-search-button']")]
        private IWebElement ClicksearchButton;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'secondary-item')]//a[contains(@class,'bold')]")]
        private IList<IWebElement> SecondaryArticles;

        [FindsBy(How = How.XPath, Using = ("//ul[@class='gs-o-list-ui--top-no-border nw-c-nav__wide-sections']//span[text()='Coronavirus']"))]
        private IWebElement CoronavirusLink;

        [FindsBy(How = How.XPath, Using = ("//li[contains(@class, 'nw-c-nav__secondary-menuitem-container')]"))]
        private IWebElement YourCoronavirusStoryLink;

        [FindsBy(How = How.XPath, Using = ("//h3[text()='Coronavirus: Send us your questions']/parent::a"))]
        private IWebElement SendUsQuestionsLink;

        [FindsBy(How = How.XPath, Using = ("//p[contains(text(),'I accept ')]"))]
        private IWebElement AcceptCheckBox;

        [FindsBy(How = How.XPath, Using = ("//button[@class='button']"))]
        private IWebElement SubmitButton;

        [FindsBy(How = How.XPath, Using = "//div[@data-entityid='container-top-stories#1']//a")]
        private IWebElement PrimaryArticleText;

        [FindsBy(How = How.XPath, Using = ("//a[@class='twite__share-link']"))]
        private IWebElement ShareLink;

        [FindsBy(How = How.XPath, Using = ("//div[contains(@class,'gs-c-promo--stacked@xxl')]//a[contains(@class,'nw-o-link-split__anchor')]"))]
        private IWebElement EntertaimentArticleTest;

        [FindsBy(How = How.XPath, Using = ("//h3[text()='How to share with BBC News']/parent::a"))]
        private IWebElement HowToShareWithBBCNewsPage;

        [FindsBy(How = How.XPath, Using = ("//*[@class='hearken-embed cleanslate']"))]
        private IWebElement submitedText;

        [FindsBy(How = How.XPath, Using = ("//*[@class='hearken-embed cleanslate']"))]
        private IWebElement ErrorMessageOnTheQuestionBBC;
        
        public NewsPage ClickOnThePopUpSingIn()
        {
            PopUpSingIn.Click();
            return this;
        }

        public NewsPage clickOnHeaderArticle()
        {
            EntertaimentArticleTest.Click();
            return this;
        }

        public string getShareLink() {
            WaitForSeconds(10);
            return ShareLink.Text; 
        }

        public string getArticleText() => ShareLink.Text;

        public string GetPrimaryArticleText() => PrimaryArticleText.Text;

        public string[] SecondaryTitleArticles()
        {
            return SecondaryArticles
                  .Select(element => element.Text)
                  .ToArray();
        }

        public string GetTextCategoryLink() => TextCategoryLink.Text;

        public NewsPage EnterTextCategoryLinkInTheSearchInput(string textcategory)
        {
            InputSearch.SendKeys(textcategory);
            ClicksearchButton.Click();
            return this;
        }

        public NewsPage ClickOnTab(string tab)
        {
            WaitForSeconds(3);
            driver.FindElement(By.XPath("//span[text()='" + tab + "']/parent::a"))
              .Click();
            return this;
        }

        public NewsPage ClickOnButton(string button)
        {
            WaitForSeconds(3);
            driver.FindElement(By.XPath("//*[text()='" + button + "']"))
                .Click();
            return this;

        }
        public NewsPage ClickOnTheElement(string elem)
        {
            WaitForSeconds(3);
            driver.FindElement(By.XPath("//h3[text()='" + elem + "']"))
                  .Click();
            return this;
        }

        public bool FirstResultArticleIsCorrect(string containsText)
        {
            return SearchResultFirstArticle.Text.Contains(containsText);
        }

        public NewsPage ClickOnTheCoronavirusTitle() {  
            CoronavirusLink.Click();
            return this;
        }

        public NewsPage ClickOnTheYourCoronavirusStoryTitle() { 
            YourCoronavirusStoryLink.Click();
            return this;
        }

        public NewsPage ClickOnTheSendUsQuestionsLink() { SendUsQuestionsLink.Click();
            return this;
        }

        public NewsPage ClickOnHowToShareWithBBCNewsPage()
        {
            WaitForSeconds(5);
            HowToShareWithBBCNewsPage.Click();
            return this;
        }
        public string GetTextHeaderArticle() => TextHeaderArticle.Text;
        private void WaitForSeconds(int second)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(second);
        }

        public Form GetForm()
        {
            return new Form(driver);
        }
        public NewsPage ClickOnTheAcceptCheckBox()
        {
            AcceptCheckBox.Click();
            return this;
        }
        public NewsPage ClickOnTheSubmitButton() 
        { 
            SubmitButton.Click(); 
            return this; 
        }
        public bool IsDisplayedrrorMessageOnTheQuestionBBC()
        {
           return ErrorMessageOnTheQuestionBBC.Displayed;
        }
    }
}
