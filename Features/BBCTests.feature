﻿Feature: BBC tests

Scenario: Share link article open
Given the BBC HomePage is open
When I go to NewsHeading
And click on "Entertainment & Arts" tab
And click the first article
And click "Share"
And copy the link
And navigate the link
Then the same article is open

Scenario: Share message to BBC
Given the BBC HomePage is open
When I go to NewsHeading
And click on "Coronavirus" tab
And click on "Your Coronavirus Stories" tab
And go to “How to share with BBC news”
And fill form share with BBC news in the information on the bottom
|Key			|Value									|
|Story			|Lorem ipsum							|
|Name			|Test									|
|Email Address	|test@test.test							|
|Contact number	|										|
|Location		|4 Privet Drive, Little Whinging, Surrey|
And click "Submit"
Then the story is not sent

Scenario: The name of the headline article the same as expected result
Given the BBC HomePage is open
When I go to NewsHeading
Then Checks the name of the headline article 

Scenario: The name of the second article the same as expected result
Given the BBC HomePage is open
When I go to NewsHeading
Then Checks secondary article titles

Scenario: ThirdTest
Given the BBC HomePage is open
When I go to NewsHeading
And Get Stores the text of the Category link of the headline article and Enter this text in the Search bar
Then Check the name of the first article against a specified value 


Scenario: Check that user can submit a qquestion to BBC
Given the BBC HomePage is open
When I go to NewsHeading
And click on "Coronavirus" tab
And click on "Your Coronavirus Stories" tab
And go to “Coronavirus: Send us your questions”
And fill form send your question in the information on the bottom 
| Key             | Value          |
| Qestion         | Lorem          |
| Name            | Test           |
| Email           | test@test.test |
| Age             | 14             |
| Postcode        | 066455         |
| TelephoneNumber | 3251515212121  |
And click "Submit"
Then the submission did not work